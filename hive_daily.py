import airflow, os
from datetime import datetime, timedelta
from airflow import DAG
from airflow.models import Connection
from airflow.contrib.operators.ssh_operator import SSHOperator
from airflow.providers.ssh.hooks.ssh import SSHHook
from airflow import settings
from airflow.models import Connection


# set connection id
ssh_name = 'hive_new_uploading'



#set args for dag
args = {
    'owner': 'apache_hive_uploading_parquet',
    'depends_on_past': False,
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0
}




#working in time and set the time of 1/1/2020 and giving the parameter of dags 
dag = DAG(
    dag_id = 'hive_uploading_parquet',
    default_args=args,
    start_date=datetime(2020, 1, 1),
    schedule_interval='0 9 * * *',
    catchup=False
)


# giving the connection id to ssh_hook
sshHook = SSHHook(ssh_conn_id="hive_local")


# creating the task 
task3 = SSHOperator(
        task_id="hive_uploading",
        command= "cd /home/hive_nyc/ && sh daily_trips.sh ",
        ssh_hook = sshHook,
        ssh_conn_id= "hive_local",
        dag = dag,
        do_xcom_push=True,
)




# running the task 3
task3



