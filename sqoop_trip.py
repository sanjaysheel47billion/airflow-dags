#importing libraries
import airflow
from datetime import datetime, timedelta
from airflow import DAG
from airflow.models import Connection
from airflow.utils import timezone
from airflow.operators.bash_operator import BashOperator
from airflow.contrib.operators.ssh_operator import SSHOperator
from airflow.providers.ssh.hooks.ssh import SSHHook


#working in time and set the time before 10 minutes
delta = timedelta(minutes=-10)
now = datetime.now()


# set connection id
ssh_name = 'ssh_default'


#set args for dag
args = {
    'owner': 'apache_sqoop_trip_folder',
    'depends_on_past': False,
    'start_date': datetime(2020, 4, 1)
}

# giving the parameter of dags 
dag = DAG(
    dag_id = 'sqoop_trip_folder',
    default_args = args,
    schedule_interval= '0 2 * * *',
)


# giving the connection id to ssh_hook
sshHook = SSHHook(ssh_conn_id=ssh_name)



# creating the task 
task3 = SSHOperator(
        task_id="sqoop_trip",
        command= "cd /home && sh sqoop_script_nyc/trip/sqoop_trip.sh ",
        ssh_hook = sshHook,
        dag = dag,
)



# running the task 3
task3














################################This is the part of testing code #######################################
# task2 = SSHOperator(
#         task_id="creating_folder_test",
#         # command= "touch alpha.txt",
#         command= "export JAVA_HOME=/usr/java/default && export PATH=$PATH:$JAVA_HOME/bin && export SQOOP_HOME=/usr/local/sqoop && export PATH=$PATH:$SQOOP_HOME/bin && export HADOOP_MAPRED_HOME=/usr/local/hadoop && export PATH=$PATH:HADOOP_MAPRED_HOME/bin && echo $JAVA_HOME && echo $PATH",
#         ssh_hook = sshHook,
#         dag = dag,
# )
########################################################################
